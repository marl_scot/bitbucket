<?php
/**
 * Set of methods for accessing BitBucket
 * Full BitBucket API is not required for this application.
 *
 * User: matt
 * Date: 06/05/18
 * Time: 15:11
 *
 * PHP version 5
 *
 * LICENSE: This source file is subject to version 3.01 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://www.php.net/license/3_01.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to license@php.net so we can mail you a copy immediately.
 *
 * @category  ACTweb/satis
 * @package   SatisBitbucketManager
 * @author    Matt Lowe <marl.scot.1@googlemail.com>
 * @copyright 2018 ACTweb
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @version   0.0.1
 * @link      http://www.actweb.info/package/SatisBitbucketManager
 */


namespace \actweb\bitbucket;

use GuzzleHttp\Client;


class bitbucket
{
    /**
     * @var string Path and filename for the Bitbucket app password
     */
    public $appPasswordAuthFile = '~/.SatisBitbucketManager.json';
    /**
     * @var string App username for Bitbucket
     */
    private $appUser = '';
    /**
     * @var string App password for Bitbucket
     */
    private $appPass = '';
    /**
     * @var array Of current repos within users account
     */
    private $repoList;
    /**
     * @var string Current slug we are working with
     */
    public $currentSlug;
    /**
     * @var string URL for API version 2.0
     */
    private $API2Url = 'https://api.bitbucket.org/2.0/';
    /**
     * @var string URL for API version 1.0
     */
    private $API1Url = 'https://api.bitbucket.org/1.0/';
    /**
     * @var mixed Result from Guzzle request (request body)
     */
    public $result;
    /**
     * @var int Status code from Guzzle request
     */
    public $statusCode;

    /**
     * @var array Current repository information
     */
    private $currentRepository;

    /**
     * @var bool Used to enable de output for the Guzzle client
     */
    private $debug = false;

    /**
     * Basic Constructor
     */
    public function __construct()
    {
        // Do nothing just now
    }

    /**
     * Calls the required method to obtain the authentication details
     *
     * @return $this
     */
    public function authenticate()
    {
        $this->getAppLoginDetails();
        return $this;
    }

    /**
     * Sets the current slug to work with
     *
     * @param $slug
     *
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setSlug($slug)
    {
        $this->currentSlug = $slug;
        $this->getRepoInfo();
        return $this;
    }

    /**
     * Loads the BitBucket app credentials from external file
     */
    private function getAppLoginDetails(): void
    {
        $jsonFile = $this->expandHomeDirectory($this->appPasswordAuthFile);
        $json = json_decode(file_get_contents($jsonFile));
        $this->appUser = $json->username;
        $this->appPass = $json->password;
    }

    /**
     * Expands 'home' symbol '~' in a path to point
     * to the correct full filesystem path.
     *
     * @param string $path Path to expand
     *
     * @return string Expanded path
     */
    public function expandHomeDirectory($path): string
    {
        $homeDir = getenv('HOME');
        if (empty($homeDir)) {
            $homeDir = getenv('HOMEDRIVE') . getenv('HOMEPATH');
        }
        return str_replace('~', realpath($homeDir), $path);
    }

    /**
     * Gets a list of all the repos for the current user
     *
     * @return array|int Array of all repos or an INT showing HTTP error code
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllUsersRepos()
    {
        $url = $this->API2Url;
        $url .= 'repositories/' . $this->appUser;
        $next = $url;
        $fullList = array();
        while ($next !== '') {
            $page = $this->getJSONfromAPI('GET', $next);
            if (!\is_array($page)) {
                // OK, we got an HTTP error code back instead of data!
                return $page;
            }
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $fullList = array_merge($fullList, $page['values']);
            if (array_key_exists('next', $page)) {
                $next = $page['next'];
            } else {
                $next = '';
            }
        }
        $this->repoList = $fullList;
        return $fullList;
    }

    /**
     * Return the repo info for the current working slug
     *
     * @return array/int Array containing repos information or error code
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getRepoInfo()
    {
        $url = $this->API2Url;
        $url .= 'repositories/' . $this->appUser . '/' . $this->currentSlug;
        $data = $this->getJSONfromAPI('GET', $url);
        $this->currentRepository=$data;
        return $data;
    }

    /**
     * Returns a list of deployment keys for the current slug
     *
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDeploymentKeys()
    {
        $url = $this->API1Url;
        $url .= 'repositories/' . $this->appUser . '/' . $this->currentSlug
            . '/deploy-keys';
        $this->getJSONfromAPI('GET', $url);
        return $this->result;
    }

    /**
     * Adds the specified SSH deployment key with a name of $name to current slug
     *
     * @param string $key  SSH Key to add to deployment keys
     * @param string $name Name to assign to SSH key for referance
     *
     * @return \actweb\bitbucket
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addDeploymentKey($key, $name = ''): bitbucket
    {
        $url = $this->API1Url;
        $url .= 'repositories/' . $this->appUser . '/' . $this->currentSlug
            . '/deploy-keys';

        $this->postAPI(
            $url, array(
                'key' => $key,
                'label' => $name
            )
        );
        return $this;
    }

    /**
     * Returns a single file from the current slug
     *
     * @param string $file     File name to fetch (including path if needed)
     * @param string $revision Revision of file to fetch or Master if none set
     *
     * @return \actweb\bitbucket
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getSingleFileFromSlug($file, $revision = 'master')
    {
        $url = $this->API1Url . 'repositories/' . $this->appUser . '/'
            . $this->currentSlug . '/raw/' . $revision . '/' . $file;
        try {
            $this->callAPI('GET', $url);
        } catch (\Exception $e) {
            //echo 'Exception error :'.$e->getMessage()."\n";
            return false;
        }
        return $this->result;
    }

    /**
     * Checks if a file exists in the current slug
     *
     * @param string $file     Filename to look for
     * @param string $revision GIT revision
     *
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fileExists($file, $revision = 'master')
    {
        $url = $this->API1Url . 'repositories/' . $this->appUser . '/'
            . $this->currentSlug . '/raw/' . $revision . '/' . $file;
        try {
            $this->callAPI('GET', $url);
        } catch (\Exception $e) {
            //echo 'Exception error :'.$e->getMessage()."\n";
            return false;
        }
        if ($this->statusCode === 200) {
            return true;
        }
        return false;
    }

    /**
     * Return an array of the request JSON body
     *
     * @noinspection ArrayTypeOfParameterByDefaultValueInspection
     *
     * @param string $type POST/GET request type
     * @param string $url  API URL to call
     *
     * @param array  $params
     *
     * @return mixed Array of data returned in request body or INT error code
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getJSONfromAPI($type, $url, $params = [])
    {
        $this->callAPI($type, $url, $params);
        if ($this->statusCode !== 200) {
            return $this->statusCode;
        }
        return json_decode($this->result, true);
    }


    /**
     * Request a basic API call to request URL with $type/$url/$data
     *
     * @noinspection ArrayTypeOfParameterByDefaultValueInspection
     *
     * @param string $type   POST/GET request type
     * @param string $url    URL to call
     * @param array  $params Data to be passed
     *
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function callAPI($type, $url, $params = [])
    {
        //$url=preg_replace('#/+#','/', $url);
        $this->debug("Guzzle request for : $type:$url");
        $client = new Client();
        $paramsSend = $this->expandParams($params);
        $this->debug('Params being sent :' . print_r($paramsSend, true));
        $res = $client->request(
            $type, $url, $paramsSend
        );
        $this->statusCode = $res->getStatusCode();
        $this->result = $res->getBody();
        return $this;
    }

    /**
     * @param string       $url    URL to post to
     * @param string|array $params Parameters to pass, if array then pass as multipart, if string pass as body
     *
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function postAPI($url, $params = '')
    {
        $data = $this->expandParams($params);
        $client = new Client();
        $res = $client->request('POST', $url, $data);
        $this->statusCode = $res->getStatusCode();
        $this->result = $res->getBody();
        return $this;
    }

    /**
     * @param string|array $params Parameters to encode
     * @param bool         $auth   If false, then DON'T merge authentication data into returned array
     *
     * @return array Encoded parameters
     */
    private function expandParams($params, $auth = true): array
    {
        $data = array();
        if (!empty($params) && \is_array($params)) {
            foreach ($params as $item => $value) {
                $data[] = array(
                    'name' => $item,
                    'contents' => $value
                );
            }
            $data = array(
                'multipart' => $data
            );
        } elseif ($params !== '' && !\is_array($params)) {
            $data = array(
                'body' => $params
            );
        }
        if ($auth) {
            $data = array_merge(
                $data,
                array(
                    'auth' => [$this->appUser, $this->appPass]
                )
            );
        }
        if ($this->debug) {
            $data = array_merge(
                $data,
                array(
                    'debug' => true
                )
            );
        }
        return $data;
    }

    /**
     * Used to enable Guzzle debugging
     *
     * @param bool $debug
     *
     * @return bitbucket
     */
    public function setDebug(bool $debug): bitbucket
    {
        $this->debug = $debug;
        return $this;
    }

    private function debug($data)
    {
        if ($this->debug) {
            print_r($data);
        }
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->currentRepository['is_private'] === 1;
    }

    /**
     * DO NOT USE
     * Set current repo visability to private
     * @todo Configure this to write private repo setting!!!!
     * @param bool $private
     */
    public function setPrivate(bool $private): void
    {
        $this->private = $private;
    }

    /**
     * @return bool
     */
    public function isPublic(): bool
    {
        return $this->currentRepository['is_private'] !== 1;
    }

    /**
     * DO NOT USE
     * Set current repo visability to public
     *
     * @todo Configure this to clear private repo setting!!!
     * @param bool $public
     */
    public function setPublic(bool $public): void
    {
        $this->public = $public;
    }

    /**
     * Define if we want to have private &/or public repo data returned.
     *
     * @param bool $pub If true, include public repos
     * @param bool $pri If true, include private repos
     *
     * @return $this
     */
    public function requiredVisibility($pub = true, $pri = true)
    {
        $this->public=$pub;
        $this->private=$pri;
        return $this;
    }
    /**
     * Checks the visibility of a repo and returns true if
     * user has set $public/$private to select that type
     * @return bool
     */
    public function checkRepoVisibility()
    {
        $repoStatus=$this->currentRepository['is_private'];
        if ($this->public && ($repoStatus !== 1)) {
            return true;
        }
        if ($this->private && ($repoStatus === 1)) {
            return true;
        }
        return false;
    }
}
